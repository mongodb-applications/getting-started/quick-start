﻿// See https://aka.ms/new-console-template for more information

using MongoDB.Driver;
using MongoDB.Bson;



internal class Program
{

    private const string connectionString = "mongodb+srv://spravce:JYHoYzXaRFMvZah5@learn-mongodb-cluster.lkbhbsw.mongodb.net/?retryWrites=true&w=majority";
    private static void Main(string[] args)
    {
        //const string connectionString = "mongodb+srv://spravce:JYHoYzXaRFMvZah5@learn-mongodb-cluster.lkbhbsw.mongodb.net/?retryWrites=true&w=majority";
        //string connectionString = "mongodb+srv://spravce:JYHoYzXaRFMvZah5@learn-mongodb-cluster.lkbhbsw.mongodb.net/?retryWrites=true&w=majority";
        
        //if (connectionString == null)
        if (string.IsNullOrEmpty(connectionString))
        {
            Console.WriteLine("You must set your 'MONGODB_URI' environment variable. To learn how to set it, see https://www.mongodb.com/docs/drivers/csharp/current/quick-start/#set-your-connection-string");

            Environment.Exit(0);
        }

        var client = new MongoClient(connectionString);

        var collection = client.GetDatabase("sample_mflix").GetCollection<BsonDocument>("movies");

        //var filter = Builders<BsonDocument>.Filter.Eq("title", "Back to the Future");
        FilterDefinition<BsonDocument> filter = Builders<BsonDocument>.Filter.Eq("title", "Back to the Future");
        
        //var document = collection.Find(filter).First();
        BsonDocument document = collection.Find(filter).First();

        Console.WriteLine(document);
                
        Console.WriteLine("Press any key...");
        Console.ReadLine();

    }
}